package com.example.basicexoplayerkotlin

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.google.android.exoplayer2.database.ExoDatabaseProvider
import com.google.android.exoplayer2.offline.Download
import com.google.android.exoplayer2.offline.DownloadManager
import com.google.android.exoplayer2.offline.DownloadService
import com.google.android.exoplayer2.scheduler.PlatformScheduler
import com.google.android.exoplayer2.scheduler.Scheduler
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor
import com.google.android.exoplayer2.upstream.cache.SimpleCache
import java.io.File

class DemoDownloadService : DownloadService(FOREGROUND_NOTIFICATION_ID_NONE) {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        // set notification
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //
        if (notificationManager.getNotificationChannel(SERVICE_NOTIFICATION_CHANNEL) == null){
            val notificationChannel = NotificationChannel(
                SERVICE_NOTIFICATION_CHANNEL,
                "Download test notifications on Exoplayer",
                NotificationManager.IMPORTANCE_LOW
            )
            //
            notificationManager.createNotificationChannel(notificationChannel)
        }
        //
        val notification = Notification.Builder(
            this,
            SERVICE_NOTIFICATION_CHANNEL
        ).apply {
            setContentTitle("Download test with Exoplayer")
            setContentText("Service is running")
            setSmallIcon(R.drawable.ic_file_download_black_24dp)
        }
        //
        startForeground(1, notification.build())
    }

    // Download Manager
    override fun getDownloadManager(): DownloadManager {
        // set ExoDatabase
        val databaseProvider = ExoDatabaseProvider(this)
        // save to directory cache
        val downloadCache = SimpleCache(
            File("${this.getExternalFilesDir(null)?.path}/cache"),
            NoOpCacheEvictor(),
            databaseProvider,
        )
        // Create a factory for reading the data from the network.
        val dataSourceFactory = DefaultHttpDataSourceFactory("@user")
        // create download manager
        val downloadManager = DownloadManager(
            this,
            databaseProvider,
            downloadCache,
            dataSourceFactory
        )
        downloadManager.addListener(object : DownloadManager.Listener {

        })
        return downloadManager
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun getScheduler(): Scheduler? {
        return PlatformScheduler(this, JOB_ID)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun getForegroundNotification(downloads: MutableList<Download>): Notification {
        //
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //
        if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL) == null){
            val notificationChannel =
                NotificationChannel(NOTIFICATION_CHANNEL, "Ready for offline playback", NotificationManager.IMPORTANCE_LOW)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        //
        val notification = Notification.Builder(
            this,
            NOTIFICATION_CHANNEL
        ).apply {
            setContentTitle("Ready for offline playback")
            setContentText(downloads.size.toString())
            setSmallIcon(R.drawable.ic_file_download_black_24dp)
        }
        return notification.build()
    }


    companion object {
        const val NOTIFICATION_CHANNEL = "cacheNotification"
        const val SERVICE_NOTIFICATION_CHANNEL = "serviceNotification"
        const val JOB_ID = 4545

    }

}