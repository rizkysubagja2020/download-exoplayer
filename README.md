#Langkah Awal
## Build Gradle
### 1. Gradle
````
// exoplayer
    implementation 'com.google.android.exoplayer:exoplayer:2.11.4'
    implementation 'com.google.android.exoplayer:exoplayer-dash:2.11.4'
    implementation 'com.google.android.exoplayer:exoplayer-hls:2.11.4'
    implementation 'com.google.android.exoplayer:exoplayer-ui:2.11.4'
    implementation 'com.google.android.exoplayer:exoplayer-smoothstreaming:2.11.4'
    implementation 'com.google.android.exoplayer:exoplayer-core:2.11.4'
````
## Buat Download Service yang berisi Download Manager
### 2. DemoDownloadService.kt
````
class DemoDownloadService : DownloadService(FOREGROUND_NOTIFICATION_ID_NONE) {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        // set notification
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //
        if (notificationManager.getNotificationChannel(SERVICE_NOTIFICATION_CHANNEL) == null){
            val notificationChannel = NotificationChannel(
                SERVICE_NOTIFICATION_CHANNEL,
                "Download test notifications on Exoplayer",
                NotificationManager.IMPORTANCE_LOW
            )
            //
            notificationManager.createNotificationChannel(notificationChannel)
        }
        //
        val notification = Notification.Builder(
            this,
            SERVICE_NOTIFICATION_CHANNEL
        ).apply {
            setContentTitle("Download test with Exoplayer")
            setContentText("Service is running")
            setSmallIcon(R.drawable.ic_file_download_black_24dp)
        }
        //
        startForeground(1, notification.build())
    }

    // Download Manager
    override fun getDownloadManager(): DownloadManager {
        // set ExoDatabase
        val databaseProvider = ExoDatabaseProvider(this)
        // save to directory cache
        val downloadCache = SimpleCache(
            File("${this.getExternalFilesDir(null)?.path}/cache"),
            NoOpCacheEvictor(),
            databaseProvider,
        )
        // Create a factory for reading the data from the network.
        val dataSourceFactory = DefaultHttpDataSourceFactory("@user")
        // create download manager
        val downloadManager = DownloadManager(
            this,
            databaseProvider,
            downloadCache,
            dataSourceFactory
        )
        downloadManager.addListener(object : DownloadManager.Listener {

        })
        return downloadManager
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun getScheduler(): Scheduler? {
        return PlatformScheduler(this, JOB_ID)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun getForegroundNotification(downloads: MutableList<Download>): Notification {
        //
        val notificationManager =
            this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //
        if (notificationManager.getNotificationChannel(NOTIFICATION_CHANNEL) == null){
            val notificationChannel =
                NotificationChannel(NOTIFICATION_CHANNEL, "Ready for offline playback", NotificationManager.IMPORTANCE_LOW)
            notificationManager.createNotificationChannel(notificationChannel)
        }
        //
        val notification = Notification.Builder(
            this,
            NOTIFICATION_CHANNEL
        ).apply {
            setContentTitle("Ready for offline playback")
            setContentText(downloads.size.toString())
            setSmallIcon(R.drawable.ic_file_download_black_24dp)
        }
        return notification.build()
    }


    companion object {
        const val NOTIFICATION_CHANNEL = "cacheNotification"
        const val SERVICE_NOTIFICATION_CHANNEL = "serviceNotification"
        const val JOB_ID = 4545

    }

}
````
## Kemudian define Download Service di manifest
````
.....
        <service
            android:name=".DemoDownloadService"
            android:exported="false">
            <!-- This is needed for Scheduler -->
            <intent-filter>
                <action android:name="com.google.android.exoplayer.downloadService.action.RESTART" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </service>
````
.....
## Kemudian set HLSnya
### 
````
private fun cacheHLS() {
        //
        val sourceFactory = DefaultDataSourceFactory(
            this,
            "@user",
            object : TransferListener {
                override fun onTransferInitializing(
                    source: DataSource,
                    dataSpec: DataSpec,
                    isNetwork: Boolean
                ) {
                    TODO("Not yet implemented")
                }

                override fun onTransferStart(
                    source: DataSource,
                    dataSpec: DataSpec,
                    isNetwork: Boolean
                ) {
                    TODO("Not yet implemented")
                }

                override fun onBytesTransferred(
                    source: DataSource,
                    dataSpec: DataSpec,
                    isNetwork: Boolean,
                    bytesTransferred: Int
                ) {
                    TODO("Not yet implemented")
                }

                override fun onTransferEnd(
                    source: DataSource,
                    dataSpec: DataSpec,
                    isNetwork: Boolean
                ) {
                    TODO("Not yet implemented")
                }
            }
        )
        //
        val downloadHelper = DownloadHelper.forHls(
            this,
            Uri.parse(HLS_STATIC_URL),
            sourceFactory,
            DefaultRenderersFactory(this)
        )
        //
        downloadHelper.prepare(object: DownloadHelper.Callback{
            override fun onPrepared(helper: DownloadHelper) {
                //
                DownloadService.sendAddDownload(
                    this@MainActivity,
                    DemoDownloadService::class.java,
                    downloadHelper.getDownloadRequest(ByteArray(1024)),
                    false
                )
                //
                downloadHelper.release()
            }

            override fun onPrepareError(helper: DownloadHelper, e: IOException) {
                TODO("Not yet implemented")
            }
        })
    }

    // object
    companion object {
        const val HLS_STATIC_URL = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
        const val STATE_RESUME_WINDOW = "resumeWindow"
        const val STATE_RESUME_POSITION = "resumePosition"
        const val STATE_PLAYER_FULLSCREEN = "playerFullscreen"
        const val STATE_PLAYER_PLAYING = "playerOnPlay"
    }
````
### Kemudian set di button download
````
button.setOnClickListener {
            cacheHLS()
        }
````
### DemoDownloadService.kt
(https://gitlab.com/rizkysubagja2020/download-exoplayer/-/blob/master/app/src/main/java/com/example/basicexoplayerkotlin/DemoDownloadService.kt)

###  MainActivity.kt
(https://gitlab.com/rizkysubagja2020/download-exoplayer/-/blob/master/app/src/main/java/com/example/basicexoplayerkotlin/MainActivity.kt)
